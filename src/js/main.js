 $(document).ready(function(){
	$('#toggle-sidenav').on('click', function(){
		$('.rollr-sidenav').addClass('show-sidenav');
		$('.overlay').css('display','block');
	});
	$('#toggle-sidenav-btn').on('click', function(){
		console.log('sidenav is opening');
		$('.rollr-sidenav').addClass('show-sidenav');
		$('.overlay').css('display','block');
	});
	$('.close-btn').on('click', function(){
		$('.rollr-sidenav').removeClass('show-sidenav');
		$('.overlay').css('display','none');
	});

	$(window).scroll(function(e){
			
			e.preventDefault();
			
			var $bodyScrollTop = $(window).scrollTop();
			var $bannerOffsetTop = $('.navbar').offset().top + $('.navbar').outerHeight();

			if($bodyScrollTop > ($bannerOffsetTop/2) ){
				$('.navbar').removeClass('navbar-default--transparent').addClass('navbar--scrolling');
				$('.navbar-default .navbar-nav>li>a').css('color','black');
				$('.navbar-right .bars').css('background-color','black');
				$('#toggle-sidenav .icon-bar').addClass('bars-scrolling');
				$('.navbar-brand>img').attr('src','img/rollr-nav-logo.png');
				$('.navbar-default .navbar-toggle .icon-bar').css('background-color','black');
			}
			else {
				$('.navbar').removeClass('navbar--scrolling').addClass('navbar-default--transparent');
				$('.navbar-default .navbar-nav>li>a').css('color','white');
				$('.navbar-brand>img').attr('src','img/logo.png');
				$('.navbar-right .bars').css('background-color','white');
				$('.navbar-default .navbar-toggle .icon-bar').css('background-color','white');
			}

	});

/*
IE Detection
*/
// var rootElem = document.documentElement;

//  rootElem.setAttribute('data-useragent',  navigator.userAgent);
//  rootElem.setAttribute('data-platform', navigator.platform );
//  rootElem.className += ((!!('ontouchstart' in window) || !!('onmsgesturechange' in window))?' touch':'');
/*
 IE Detection - end
*/
flexibility(document.documentElement);

console.log(navigator.userAgent);
});